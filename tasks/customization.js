/*
 * grunt-customization
 * https://github.com/roycwc/grunt-customization
 *
 * Copyright (c) 2017 roycwc
 * Licensed under the GPL-3.0 license.
 */

'use strict';

var md5 = require('md5');

module.exports = function(grunt) {

  grunt.registerMultiTask('customization', 'Grunt plugin to inject html as template variables or js variables in script tag', function() {

    var hogan = require('hogan.js');
    var fs = require('fs');
    var options = this.options({
      envFile:'.customize.json',
      customize:{envVar:{},replace:{}}
    });
    
    if (!grunt.file.exists(options.envFile)){
      return;
    }

    options.customize = JSON.parse(fs.readFileSync(options.envFile,'utf8'));
    
    this.filesSrc.forEach(function(f){
      if (grunt.file.exists(f)){
        var indexHtml = fs.readFileSync(f,'utf8');
        var replacesAndEnvVars = options.customize.replace;
        options.customize.envVar._rel = md5(new Date()).slice(0,4);
        replacesAndEnvVars._envVar = '<script type="text/javascript"> var _envVar = '+JSON.stringify(options.customize.envVar)+'</script>';
        var template = hogan.compile(indexHtml, {delimiters: '{{{ }}}'});
        fs.writeFileSync(f, template.render(replacesAndEnvVars));
      }
    });
  });
};
