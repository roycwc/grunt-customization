'use strict';

var fs = require('fs');
var customize = JSON.parse(fs.readFileSync('test/.customize.json','utf8'));

exports.customization = {
  replace_one_file: function(test) {
    test.expect(1);
    var indexHtml = fs.readFileSync('tmp/index.html','utf8');
    test.ok(indexHtml.indexOf(customize.replace.title)>=0);
    test.done();
  },
  replace_recursively: function(test){
    test.expect(1);
    var indexHtml = fs.readFileSync('tmp/views/baby.html','utf8');
    test.ok(indexHtml.indexOf(customize.replace.baby)>=0);
    test.done();
  },
  inject_envVar: function(test){
    test.expect(1);
    var indexHtml = fs.readFileSync('tmp/index.html','utf8');
    test.ok(indexHtml.indexOf('_envVar')>0);
    test.done();
  },
  has_release_hash: function(test){
    test.expect(1);
    var indexHtml = fs.readFileSync('tmp/index.html','utf8');
    test.ok(indexHtml.indexOf('_rel')>0);
    test.done();
  }
};
