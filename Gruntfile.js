/*
 * grunt-customization
 * https://github.com/roycwc/grunt-customization
 *
 * Copyright (c) 2017 roycwc
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    jshint: {
      all: [
        'Gruntfile.js',
        'tasks/*.js',
        '<%= nodeunit.tests %>'
      ],
      options: {
        jshintrc: '.jshintrc'
      }
    },

    // Before generating any new files, remove any previously-created files.
    clean: {
      tests: ['tmp']
    },

    // Configuration to be run (and then tested).
    customization: {
      default: {
        options: {
          envFile:'test/.customize.json'
        },
        src: ['tmp/index.html', 'tmp/views/**/*.html']
      }
    },

    // Unit tests.
    nodeunit: {
      tests: ['test/*_test.js']
    }

  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-nodeunit');
  
  grunt.registerTask('copy', 'Copy fixtures to a tmp folder.', function() {
    grunt.file.copy('test/fixture/index.html', 'tmp/index.html');
    grunt.file.copy('test/fixture/views/baby.html', 'tmp/views/baby.html');
  });

  grunt.registerTask('test', ['clean', 'copy', 'customization', 'nodeunit']);
};
