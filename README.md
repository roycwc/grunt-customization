# grunt-customization

> Grunt plugin to inject html as template variables or js variables in script tag

[![Build Status](https://travis-ci.org/roycwc/grunt-customization.svg?branch=master)](https://travis-ci.org/roycwc/grunt-customization)

## Getting Started
This plugin requires Grunt `~0.4.5`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install roycwc/grunt-customization --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-customization');
```

## The "customization" task

### Overview
In your project's Gruntfile, add a section named `customization` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
  customization: {
    options: {
      src:'./.tmp/index.html', // your src file
      envFile:'./.customize.json', // your env variable json file 
    }
  },
});
```

## Test
```
npm run test
```